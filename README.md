# asmbuilder

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

An assembler for x86_32.

This project contains two parts:
- An assembler library, capable of producing x86_32 output in either raw binary format, COFF .obj binary format, and/or NASM text format
- An assembler binary around it, which parses assembly input files and calls into the above library

It's intended to be useful either as a standalone assembler, or integrated as a library into part of a larger toolchain.

Tags: PL

## Changelog

2016-01-24: r44
- Public release of last private release
- [⬇️ asmbuilder-r44.zip](dist-archive/asmbuilder-r44.zip) *(16.06 KiB)*


2015-09-09: r44-private
- Initial private release

2015-08-01: r0
- Initial commit
